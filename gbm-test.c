/*
 * Copyright (C) 2019 DisplayLink (UK) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gbm.h>
#include <drm_fourcc.h>

/*
 * This program tests for GEM handle leaks in GBM.
 *
 * It allocates a GBM BO on one DRM device, exports it as dmabuf,
 * imports the dmabuf to another DRM device via GBM, and prints the
 * final handle. This is repeated many times.
 *
 * The imported should be a display-only DRM device, because that code
 * in Mesa GBM was suspected to leak GEM handles. If GEM handles are
 * leaked, the allocating device may run out of memory while running
 * this program. If GEM handles are leaked, all printed handle values
 * will be different, but I'm not sure if the converse is true.
 *
 * Therefore this should be ran up to memory exhaustion if possible.
 *
 * If all printed GEM handles are the same, no leaks are possible.
 */

/*
 * ALLOC_DEVICE can probably be anything, as long as it works and is
 * not the same as IMPORT_DEVICE.
 *
 * IMPORT_DEVICE should be a display-only device, e.g. VKMS, EVDI.
 */
#define ALLOC_DEVICE "/dev/dri/card0"
#define IMPORT_DEVICE "/dev/dri/card1"

/* Buffer parameters */
#define WIDTH 1024
#define HEIGHT 1024
#define FORMAT DRM_FORMAT_XRGB8888
#define MODIFIER DRM_FORMAT_MOD_LINEAR

/* How many times allocate, import, and free. */
#define REPEATS 1000


static struct gbm_bo *
import(struct gbm_device *import_dev,
       int dmabuf_fd, struct gbm_bo *source_bo)
{
	struct gbm_import_fd_data data = {
		.fd     = dmabuf_fd,
		.width  = gbm_bo_get_width(source_bo),
		.height = gbm_bo_get_height(source_bo),
		.stride = gbm_bo_get_stride(source_bo),
		.format = gbm_bo_get_format(source_bo),
	};

	return gbm_bo_import(import_dev, GBM_BO_IMPORT_FD, &data,
			     GBM_BO_USE_SCANOUT);
}

static int
test_cycle(struct gbm_device *alloc_dev, struct gbm_device *import_dev)
{
	uint64_t modifier = MODIFIER;
	struct gbm_bo *source_bo = NULL;
	struct gbm_bo *imported_bo = NULL;
	int dmabuf_fd = -1;
	int ret = 0;

	source_bo = gbm_bo_create_with_modifiers(alloc_dev,
						 WIDTH, HEIGHT,
						 FORMAT, &modifier, 1);
	if (!source_bo) {
		printf("Create GBM bo failed.\n");
		ret = -1;
		goto out;
	}

	if (gbm_bo_get_plane_count(source_bo) != 1)
		abort();

	dmabuf_fd = gbm_bo_get_fd(source_bo);
	if (dmabuf_fd < 0) {
		printf("Export dmabuf failed.\n");
		ret = -1;
		goto out;
	}

	imported_bo = import(import_dev, dmabuf_fd, source_bo);
	if (!imported_bo) {
		printf("Import dmabuf failed.\n");
		ret = -1;
		goto out;
	}

	printf("GEM handle of imported buffer: %u\n",
		gbm_bo_get_handle(imported_bo).u32);
	/* The handle is owned by imported_bo. */

out:
	if (imported_bo)
		gbm_bo_destroy(imported_bo);

	if (dmabuf_fd != -1)
		close(dmabuf_fd);

	if (source_bo)
		gbm_bo_destroy(source_bo);

	return ret;
}

static struct gbm_device *
device_open(const char *node, int *device_fd)
{
	struct gbm_device *gbm_dev;

	*device_fd = open(node, O_CLOEXEC);
	if (*device_fd == -1) {
		perror("open");
		return NULL;
	}

	gbm_dev = gbm_create_device(*device_fd);
	if (!gbm_dev) {
		perror("gbm_create_device");
		return NULL;
	}

	return gbm_dev;
}

int
main(void)
{
	int alloc_dev_fd = -1;
	struct gbm_device *alloc_dev;
	int import_dev_fd = -1;
	struct gbm_device *import_dev;
	int i;

	setbuf(stdout, NULL);

	alloc_dev = device_open(ALLOC_DEVICE, &alloc_dev_fd);
	if (!alloc_dev)
		return -1;

	printf("Allocator device with backend '%s'\n",
		gbm_device_get_backend_name(alloc_dev));

	import_dev = device_open(IMPORT_DEVICE, &import_dev_fd);
	if (!import_dev)
		return -1;

	printf("Importer device with backend '%s'\n",
		gbm_device_get_backend_name(import_dev));

	for (i = 0; i < REPEATS; i++)
		if (test_cycle(alloc_dev, import_dev) < 0)
			break;

	printf("Completed %d test cycles.\n", i);

	gbm_device_destroy(import_dev);
	close(import_dev_fd);

	gbm_device_destroy(alloc_dev);
	close(alloc_dev_fd);

	return 0;
}
